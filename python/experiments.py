import pickle
import os
import time

import pandas as pd
from homebrew_algorithm import AutoMLLearner

from preprocessing import load_preprocessed_crabs, load_preprocessed_traffic, load_preprocessed_stocks

import autosklearn
from sklearn.linear_model import Lasso
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from autosklearn.regression import AutoSklearnRegressor
from autosklearn.metrics import root_mean_squared_error

import tpot
from tpot import TPOTRegressor

# One hour timeout in seconds
TIMEOUT = 60
TIMEOUT_MODEL = 30
N_JOBS = 1
N_SEARCHER_PER_ALGO = 1

# print('using auto-sklearn', autosklearn.__version__)
# print('using TPOT', tpot.__version__)

datasets = {
    'crabs': load_preprocessed_crabs,
    'traffic': load_preprocessed_traffic,
    'stocks': load_preprocessed_stocks
}

subspace_wrappers =[
            {
                'learner': Lasso,

                'hp_spaces': {
                    'alpha': {
                        'type': 'float',
                        'max':5,
                        'min': 0.1
                    },
                    'selection': {
                        'type': 'categorical',
                        'values': ['cyclic', 'random']
                    }
                }
            },
            {
                'learner': SVR,

                'hp_spaces': {
                    'C': {
                        'type': 'float',
                        'max': 50,
                        'min': 0.1
                    },
                    'kernel': {
                        'type': 'categorical',
                        'values': ['linear', 'poly', 'rbf', 'sigmoid']
                    }
                }
            },
            {
                'learner': KNeighborsRegressor,

                'hp_spaces': {
                    'n_neighbors': {
                        'type': 'integer',
                        'max': 50,
                        'min': 1
                    },
                    'weights': {
                        'type': 'categorical',
                        'values': ['uniform', 'distance']
                    },
                    'p': {
                        'type': 'integer',
                        'max': 3,
                        'min': 1
                    }


                }
            }
    ]


class Exectime:
    def __enter__(self):
        self.time = time.time()
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.time = time.time() - self.time

def run_experiments():
    for dataset in datasets:

        print(f'Start processing of {dataset} dataset')
        # Load dataset
        X_train, X_test, y_train, y_test = datasets[dataset]()


        print(f'Running auto-sklearn for {dataset}')
        # Run experiments with auto-sklearn
        # https://automl.github.io/auto-sklearn/master/api.html#regression
        auto_sklearn = AutoSklearnRegressor(
            time_left_for_this_task=TIMEOUT,
            per_run_time_limit=TIMEOUT_MODEL,
            resampling_strategy='cv',
            resampling_strategy_arguments={'cv': {'folds': 5}},
            metric=root_mean_squared_error,
            scoring_functions=[root_mean_squared_error],
            n_jobs=N_JOBS
        )
        with Exectime() as exect:
            auto_sklearn.fit(X_train, y_train, dataset_name=dataset)
        print(f'Rutime auto-sklearn for {dataset} took {exect.time}')

        pickle.dump(auto_sklearn, open(os.path.join('results', f'{dataset}_sklearn.p'), 'wb'))



        # Run the experiments with TPOT 🫖
        # http://epistasislab.github.io/tpot/api/#regression
        
        tpot = TPOTRegressor(
            max_time_mins=TIMEOUT / 60,
            max_eval_time_mins=TIMEOUT_MODEL / 60,
            n_jobs=N_JOBS,
            scoring='neg_mean_squared_error',
            cv=5,
            random_state=42)

        print(f'Running TPOT for {dataset}')
        with Exectime() as exect:
            tpot.fit(X_train, y_train)

        print(f'Rutime TOPT {dataset} took {exect.time}')
        pickle.dump(tpot.fitted_pipeline_, open(os.path.join('results', f'{dataset}_tpot_fitted_pipeline.p'), 'wb'))
        pickle.dump(tpot.evaluated_individuals_, open(os.path.join('results', f'{dataset}_tpot_evaluated_individuals.p'), 'wb'))
        tpot.export(f'results/tpot_{dataset}_pipeline.py')



        print(f'Running our implementation for {dataset}')
        learner = AutoMLLearner(subspace_wrappers)
        # Run experiments with own pipeline
        with Exectime() as exect:
            errors = learner.fit(X_train, y_train, runtime=TIMEOUT / 60, n_searchers_per_algo=N_SEARCHER_PER_ALGO)

        print(f'Rutime our implementation {dataset} took {exect.time}')

        pickle.dump(errors, open(os.path.join('results', f'{dataset}_homebrew.p'), 'wb'))


        # Save test set 
        if not isinstance(X_test, pd.DataFrame):
            X_test = pd.DataFrame(X_test)

        if isinstance(y_test, pd.DataFrame):
            y_test = y_test.squeeze()

        X_test['target'] = y_test.tolist()
        X_test.to_csv(f'results/test_{dataset}.csv')


if __name__ == '__main__':
    run_experiments()