import time
import numpy as np
import sklearn
from sklearn.model_selection import cross_validate
from sklearn.datasets import make_regression
from matplotlib import pyplot as plt

from sklearn.linear_model import Lasso
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR

class modelWrapper():
    def __init__(self, subspace_wrapper, initial_params=None):
        self.subspace_wrapper = subspace_wrapper
        self.clazz = subspace_wrapper['learner']
        self.model = self.clazz()
        self.subspace = subspace_wrapper['hp_spaces']
        self.rate_of_improvement = 0.1
        self.error = None
        self.cooldown = 0
        self.no_improvement_count = 0

        #Filling randomly
        if initial_params == None:
            self.hp_values = {}
            for hp_key in self.subspace.keys():
                if self.subspace[hp_key]['type'] == 'categorical':
                    new_value = np.random.choice(self.subspace[hp_key]['values'])
                elif self.subspace[hp_key]['type'] == 'integer':
                    new_value = np.random.randint(self.subspace[hp_key]['min'], self.subspace[hp_key]['max'])
                elif self.subspace[hp_key]['type'] == 'float':
                    new_value =  np.random.uniform(low=self.subspace[hp_key]['min'], high=self.subspace[hp_key]['max'])
                self.hp_values[hp_key] = new_value
        #filling with user-provided params
        else:
            self.hp_values = initial_params

        self.model.set_params(**self.hp_values)


class AutoMLLearner():

    def __init__(self, subspace_wrappers):
        """
               :param subspace_wrappers:
                   {
                   'learner': the actual sklearan model class
                   'hp_spaces':{
                       'some_numeric_hp':{
                           'type':'integer'
                           'max':100
                           'min':1
                       }
                       'some_categorical_hp':{
                           'type': 'categorical'
                           'values': ['a','b','c']
                       }
                   }
       """

        self.subspace_wrappers = subspace_wrappers
        self.algo_space = [subspace_wrapper['learner'] for subspace_wrapper in self.subspace_wrappers]



    def get_next_step(self, incumbent, search_rate):
        #TODO make dynamic search rate
        dir_change_key = np.random.choice(list(incumbent.subspace.keys()))

        #If we're changing a categorical param, we pick a different value at random
        if incumbent.subspace[dir_change_key]['type'] == 'categorical':
             possible_values = incumbent.subspace[dir_change_key]['values'].copy()
             possible_values.remove(incumbent.hp_values[dir_change_key])
             next_value = np.random.choice(possible_values)

        #If we're changing an integer param, we randomly choose between +3 and -3
        elif incumbent.subspace[dir_change_key]['type'] == 'integer':

            maxval = incumbent.subspace[dir_change_key]['max']
            minval = incumbent.subspace[dir_change_key]['min']

            next_value = None
            search_rate = int(np.round((maxval - minval) / 20))

            while next_value == None or ((next_value<minval) or (next_value>maxval)):
                next_value = incumbent.hp_values[dir_change_key] + np.random.choice(list(range(-search_rate,search_rate+1)))

        #If we're chanigng a continuous param, we add an offset proportional to the user-provided search rate and
        #inversely proportional to the calculated rate_of_improvement
        elif incumbent.subspace[dir_change_key]['type'] == 'float':
            maxval = incumbent.subspace[dir_change_key]['max']
            minval = incumbent.subspace[dir_change_key]['min']

            next_value = None
            while next_value == None or ((next_value<minval) or (next_value>maxval)):

                delta_multiplier = np.random.normal(1, 1.5)
                if search_rate == 'dynamic': #otherwise, uses the value directly. Should be numeric.
                    search_rate = (maxval-minval)/100


                next_value = incumbent.hp_values[dir_change_key] + delta_multiplier * (search_rate/incumbent.rate_of_improvement)

        # if dir_change_key=='alpha':
        #     print(f"Alpha:{next_value}")

        new_candidate_initial_params = incumbent.hp_values
        new_candidate_initial_params[dir_change_key] = next_value
        new_candidate = modelWrapper(incumbent.subspace_wrapper, initial_params=new_candidate_initial_params)

        return new_candidate



    def fit(self, X, y, runtime=2, n_searchers_per_algo=1, search_rate = 'dynamic', n_folds=5, patience=10, cooldown_length=5, verbose=True):

        runtime_seconds = runtime*60

        #start incumbents
        incumbents = []
        error_histories = {}
        for subspace_wrapper in self.subspace_wrappers:
            for searcher_idx in range(n_searchers_per_algo):
                incumbents.append(modelWrapper(subspace_wrapper))
                error_histories[f"{subspace_wrapper['learner']}-{searcher_idx}"] = []


        iteration_idx = 0
        evaluation_ctr = 0
        start_time = time.perf_counter()
        current_time = time.perf_counter()

        new_incumbents_count = 0

        while current_time-start_time < runtime_seconds:
            iteration_idx+=1
            if verbose:
                print(f'Starting Iteration {iteration_idx}')
                print(f'\tTime elapsed: {np.round(time.perf_counter()-start_time)/60} minutes')
                print(f'\t{evaluation_ctr} models evaluated so far')

            for incumbent_idx, incumbent in enumerate(incumbents):

                if incumbent.cooldown == 0:
                    evaluation_ctr +=1

                    incumbent_candidate = self.get_next_step(incumbent, search_rate)

                    scores = cross_validate(incumbent_candidate.model, X, y, cv=n_folds, scoring ='neg_root_mean_squared_error')
                    mean_neg_rmse = np.mean(scores['test_score'])
                    incumbent_candidate.error = mean_neg_rmse

                    searcher_idx = list(error_histories.keys())[incumbent_idx]


                    #first case:
                    if incumbent.error == None:
                        if verbose:
                            print('First incumbent for searcher')
                        incumbents[incumbent_idx] = incumbent_candidate
                        new_incumbents_count += 1
                        error_histories[searcher_idx].append(incumbent_candidate.error)

                    elif incumbent_candidate.error > incumbent.error: #We are using negative of RMSE
                        if verbose:
                            print(f'Found new incumbent for searcher #{searcher_idx}')

                        incumbent_candidate.rate_of_improvement = incumbent_candidate.error - incumbent.error
                        incumbents[incumbent_idx] = incumbent_candidate

                        new_incumbents_count += 1
                        error_histories[searcher_idx].append(incumbent_candidate.error)

                    else:
                        incumbent.no_improvement_count = incumbent.no_improvement_count + 1
                        error_histories[searcher_idx].append(incumbent.error)
                        if incumbent.no_improvement_count > patience:
                            if verbose:
                                print(f'Searcher #{searcher_idx} went into cooldown')
                            incumbent.cooldown = cooldown_length

                else:
                    # error_histories[searcher_idx].append(incumbent.error)

                    incumbent.cooldown = incumbent.cooldown-1
                    if incumbent.cooldown == 0:
                        if verbose:
                            print(f'Searcher #{searcher_idx} left cooldown')
                        incumbent.no_improvement_count = 0

            if verbose:
                print(f'\t {new_incumbents_count} new incumbents found.')
            current_time = time.perf_counter()

        best_config_idx = np.argmax([incumbent.error for incumbent in incumbents])

        return {
            'error_histories':error_histories,
            'incumbents':incumbents,
            'overall_incumbent':incumbents[best_config_idx],
            'overall_incumbent_subspace_wrapper':incumbents[best_config_idx].subspace_wrapper
        }


#example usage:

# subspace_wrappers =[
#             {
#                 'learner': Lasso,

#                 'hp_spaces': {
#                     'alpha': {
#                         'type': 'float',
#                         'max':5,
#                         'min': 0.1
#                     },
#                     'selection': {
#                         'type': 'categorical',
#                         'values': ['cyclic', 'random']
#                     }
#                 }
#             },
#             {
#                 'learner': SVR,

#                 'hp_spaces': {
#                     'C': {
#                         'type': 'float',
#                         'max': 50,
#                         'min': 0.1
#                     },
#                     'kernel': {
#                         'type': 'categorical',
#                         'values': ['linear', 'poly', 'rbf', 'sigmoid']
#                     }
#                 }
#             },
#             {
#                 'learner': KNeighborsRegressor,

#                 'hp_spaces': {
#                     'n_neighbors': {
#                         'type': 'integer',
#                         'max': 50,
#                         'min': 1
#                     },
#                     'weights': {
#                         'type': 'categorical',
#                         'values': ['uniform', 'distance']
#                     },
#                     'p': {
#                         'type': 'integer',
#                         'max': 3,
#                         'min': 1
#                     }


#                 }
#             }
#     ]


# learner = AutoMLLearner(subspace_wrappers)

# X, y = make_regression(n_samples=1000, n_features=5, random_state=0)

# #5 minutes, 6*3 agents
# errors = learner.fit(X, y, runtime=5, n_searchers_per_algo=6)


#Plotting output
# plt.figure()
# for key in errors['error_histories'].keys():
#     plt.plot(errors['error_histories'][key], label=f'searcher {key}')

# plt.ylabel('-1 * RMSE')
# plt.xlabel('iteration')
# plt.legend()
# plt.show()


