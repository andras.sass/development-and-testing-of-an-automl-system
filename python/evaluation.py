from autosklearn.estimators import AutoSklearnRegressor
import pandas as pd
import pickle
from preprocessing import load_preprocessed_crabs, load_preprocessed_traffic, load_preprocessed_stocks

import matplotlib as plt
import numpy as np

datasets = {
    # 'crabs': load_preprocessed_crabs,
    # 'traffic': load_preprocessed_traffic,
    'stocks': load_preprocessed_stocks
}


def evaluate():
    for dataset in datasets:
        # Load data from previous train/test split
        X_test = pd.read_csv(f'test_{dataset}.csv')
        y_test = X_test[['target']]
        X_test.drop(columns=['target'], inplace=True)

        # Evaluate auto-sklearn 
        with open(f'results/{dataset}_sklearn.p', 'rb') as input:
            auto_sklearn: AutoSklearnRegressor = pickle.load(input)
        print(auto_sklearn.leaderboard())
        print(auto_sklearn.cv_results_)
        print(auto_sklearn.automl_.runhistory_.data)
        # Evaluate TPOT
        # Evaluate our implemenation
        break

if __name__ == '__main__':
    evaluate()