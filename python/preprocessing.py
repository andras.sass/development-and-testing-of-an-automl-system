from sklearn.preprocessing import OneHotEncoder
import numpy as np
import pandas as pd
from pandas.api.types import CategoricalDtype

from sklearn.impute import KNNImputer
from sklearn.model_selection import train_test_split
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler, RobustScaler, PowerTransformer
from scipy.io import arff


def load_clean_traffic(data):
    ds = pd.read_csv(data)

    ds.drop(["rain_1h"], axis=1, inplace=True)
    ds.drop(["weather_description"], axis=1, inplace=True)

    remove_positions = ds["temp"] == 0
    ds = ds.drop(np.where(remove_positions)[0])

    ds["date_time"] = pd.to_datetime(ds["date_time"])
    ds["holiday"] = 0 + 1 * (ds["holiday"] != "None")

    weather_main_one_hot = pd.get_dummies(ds.weather_main)
    ds = pd.concat([ds, weather_main_one_hot], axis=1)
    ds.drop(["weather_main"], axis=1, inplace=True)

    ds = ds.groupby(["holiday", "temp", "snow_1h", "clouds_all",
                     "date_time", "traffic_volume"]).sum().reset_index()
    ds = ds.drop_duplicates(subset=['date_time'])
    ds.sort_values(by=['date_time'], inplace=True)
    ds.reset_index(inplace=True)

    ds["year"] = ds['date_time'].dt.year - 2012
    months = ds['date_time'].dt.month - 1
    weekdays = ds['date_time'].dt.weekday
    hours = ds['date_time'].dt.hour

    ds.drop(["date_time"], axis=1, inplace=True)

    def to_cyclical(a_series, series_max):
        sin_ = pd.Series(np.sin(2 * np.pi * a_series /
                                series_max), name=f'{a_series.name}_sin')
        cos_ = pd.Series(np.cos(2 * np.pi * a_series /
                                series_max), name=f'{a_series.name}_cos')

        return sin_, cos_

    month_sin, month_cos = to_cyclical(months, 11)
    ds['month_sin'] = month_sin
    ds['month_cos'] = month_cos
    weekday_sin, weekday_cos = to_cyclical(weekdays, 6)
    ds['weekday_sin'] = weekday_sin
    ds['weekday_cos'] = weekday_cos
    hour_sin, hour_cos = to_cyclical(hours, 23)
    ds['hour_sin'] = hour_sin
    ds['hour_cos'] = hour_cos

    return ds[["traffic_volume", "year", "month_sin", "month_cos", "weekday_sin", "weekday_cos", "hour_sin", "hour_cos",
               "holiday", "temp", "snow_1h", "clouds_all", "Clear", "Clouds", "Drizzle", "Fog", "Haze", "Mist", "Rain",
               "Smoke", "Snow", "Squall", "Thunderstorm"]]


def load_preprocessed_traffic(data='data/Metro_Interstate_Traffic_Volume.csv', normalize=True):
    ds = load_clean_traffic(data)
    X, y = ds.iloc[:, 1:], ds.iloc[:, 0]
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42, shuffle=True)
    if normalize:
        scaler = ColumnTransformer([('scaler', StandardScaler(), ["year", "month_sin", "month_cos", "weekday_sin", "weekday_cos",
                                                                  "hour_sin", "hour_cos", "holiday", "temp", "snow_1h", "clouds_all"])],
                                   remainder='passthrough')

        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)

    return X_train, X_test, y_train, y_test


def load_preprocessed_crabs(data='data/CrabAgePrediction.csv', normalize=True):
    df = pd.read_csv(data)

    # One-Hot-Encode gender of crab
    onehot = OneHotEncoder()
    df = df.join(pd.DataFrame(onehot.fit_transform(df[['Sex']]).toarray()))
    df.rename(
        columns={0: "F", 1: "I", 2: "M"}, inplace=True)

    # Drop `Sex` column
    df.drop(columns=['Sex'], inplace=True)

    X = df.drop(columns=['Age'])
    y = df[['Age']]
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42, shuffle=True)
    
    if normalize:
        scaler = ColumnTransformer([('scaler', StandardScaler(), ["Length", "Diameter", "Height", "Weight", "Shucked Weight", 
                                                                  "Viscera Weight", "Shell Weight"])],
                                   remainder='passthrough')

        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)

    return X_train, X_test, y_train, y_test


def load_preprocessed_stocks(data='data/Processed_NYSE.csv', normalize=False):
    nyse_df = pd.read_csv(data)

    # Shifting targets and date
    nyse_df = nyse_df.drop(['EMA_200', 'Name'], axis=1)
    nyse_df[['Volume', 'Close', 'Date']] = nyse_df[['Volume', 'Close', 'Date']].shift(-1)

    # parsing date variables
    nyse_df['year'] = (pd.to_datetime(list(nyse_df['Date'])).year) - 2010
    nyse_df['month'] = (pd.to_datetime(list(nyse_df['Date'])).month)
    nyse_df['day'] = (pd.to_datetime(list(nyse_df['Date'])).day)
    nyse_df.dropna(inplace=True)

    # Dropping String-date, target variable and a pointless variable
    X = nyse_df.drop(['Close', 'Date'], axis=1)

    # Filling the remaining nans with backward fill and forward fill, in that order
    X = X.fillna(method='bfill').fillna(method='ffill')

    # Grabbing the target variable
    y = nyse_df['Close']

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42, shuffle=True)
    
    if normalize:
        scaler = ColumnTransformer([('scaler', StandardScaler(), ['AAPL','AMZN','AUD','Brent','CAC-F','CAD','CHF','CNY',
                                                                  'copper-F','CTB1Y','CTB3M','CTB6M','DAAA','DAX-F',
                                                                  'day','DBAA','DE1','DE2','DE4','DE5','DE6','DGS5',
                                                                  'DGS10','DJI-F','Dollar index','Dollar index-F',
                                                                  'DTB3','DTB4WK','DTB6','EMA_10','EMA_20','EMA_50',
                                                                  'EUR','FCHI','FTSE','FTSE-F','GAS-F','GBP','GDAXI',
                                                                  'GE','Gold','gold-F','HSI','HSI-F','IXIC','JNJ',
                                                                  'JPM','JPY','KOSPI-F','mom','mom1','mom2','mom3',
                                                                  'month','MSFT','NASDAQ-F','Nikkei-F','NZD','Oil',
                                                                  'ROC_5','ROC_10','ROC_20','RUSSELL-F','RUT',
                                                                  'silver-F','S&P','SSEC','TE1','TE2','TE3','TE5',
                                                                  'TE6','Volume','WFC','wheat-F','WIT-oil','XAG','XAU',
                                                                  'XOM','year','ROC_15','DJI','S&P-F'])],
                                   remainder='passthrough')

        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)
        
    return X_train, X_test, y_train, y_test